#define FUSE_USE_VERSION 30

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include <fuse_lowlevel.h>
#include <openssl/sha.h>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "google/bytestream/bytestream.grpc.pb.h"
#include "google/devtools/remoteexecution/v1test/remote_execution.grpc.pb.h"

using namespace google::bytestream;
using namespace google::devtools::remoteexecution::v1test;

#define MAX_OPEN_INODES 512

// #define DEBUG

struct inode;

struct inode {
	/* links for list of open inodes */
	struct inode *next, *prev;

	struct inode *parent;
	struct stat attr;
	int fd;
	Digest digest;
	std::map<std::string, struct inode *> dentries;
	bool dentries_populated;
	bool modified; /* modified/created */
	std::string temppath; /* only for modified/created files */
	std::string symlink_target;
};

struct fs_opts {
	const char *remote_url;
	const char *server_cert;
	const char *client_key;
	const char *client_cert;
	const char *local_path;
	const char *input_digest;
	const char *output_digest;
	const char *chdir;
	const char *mountpoint;
};

static struct fs {
	struct fs_opts opts;
	int local_dfd;
	struct inode root_inode;
	int n_open_inodes;
	struct inode *open_inodes;

	uid_t uid;
	gid_t gid;

	struct fuse_session *fuse;

	pid_t bwrap_pid;
	int bwrap_status;

	std::shared_ptr<grpc::Channel> channel;
	std::unique_ptr<ByteStream::Stub> bytestream_client;
	std::unique_ptr<ContentAddressableStorage::Stub> cas_client;
} fs;

struct opendir {
	std::map<std::string, struct inode *>::iterator iterator;
	long offset;
};

static void close_inode(struct inode *inode)
{
	assert(inode->fd >= 0);

	close(inode->fd);
	inode->fd = -1;

	fs.n_open_inodes--;

	if (fs.n_open_inodes == 0) {
		fs.open_inodes = NULL;
	} else {
		inode->prev->next = inode->next;
		inode->next->prev = inode->prev;
		if (fs.open_inodes == inode) {
			fs.open_inodes = inode->next;
		}
	}
	inode->prev = NULL;
	inode->next = NULL;
}

static std::string objdir(const Digest& digest)
{
	std::string path("objects/");
	path.append(digest.hash().substr(0, 2));
	return path;
}

static std::string objpath(const Digest& digest)
{
	std::string path(objdir(digest));
	path.append("/");
	path.append(digest.hash().substr(2));
	return path;
}

static int fd_from_digest(const Digest& digest)
{
	int fd = openat(fs.local_dfd, objpath(digest).c_str(), O_RDONLY);
	if (fd >= 0) {
		return fd;
	}

	if (errno != ENOENT) {
		/* I/O error */
		return -1;
	}

	/* File not in local cache, download it from the CAS server */

	if (!fs.opts.remote_url) {
		/* no CAS server */
		return -1;
	}

	std::string resource_name;
	resource_name.append(digest.hash());
	resource_name.append("/");
	resource_name.append(std::to_string(digest.size_bytes()));

	std::string temppath("/dev/fd/");
	temppath.append(std::to_string(fs.local_dfd));
	temppath.append("/tmp/XXXXXX");
	fd = mkstemp(&temppath[0]);
	if (fd < 0) {
		return -1;
	}
	FILE *fp = fdopen(fd, "wb");
	if (!fp) {
		close(fd);
		return -1;
	}

	grpc::ClientContext context;
	ReadRequest request;
	request.set_resource_name(resource_name);
	request.set_read_offset(0);
	auto reader = fs.bytestream_client->Read(&context, request);
	ReadResponse response;
	while (reader->Read(&response)) {
		fwrite(response.data().c_str(), 1, response.data().length(), fp);
		// TODO error handling
	}

	fclose(fp);

	mkdirat(fs.local_dfd, objdir(digest).c_str(), 0755);
	if (renameat(fs.local_dfd, temppath.c_str(), fs.local_dfd, objpath(digest).c_str()) < 0) {
		return -1;
	}

	fd = openat(fs.local_dfd, objpath(digest).c_str(), O_RDONLY);
	assert(fd >= 0);

	// TODO verify size matches digest

	return fd;
}

static struct inode *get_inode(fuse_ino_t inode_id)
{
	if (inode_id == FUSE_ROOT_ID) {
		return &fs.root_inode;
	} else {
		return (struct inode *) inode_id;
	}
}

static void inode_init(struct inode *inode, struct inode *parent)
{
	inode->parent = parent;
	inode->fd = -1;

	if (inode == &fs.root_inode) {
		inode->attr.st_ino = FUSE_ROOT_ID;
	} else {
		inode->attr.st_ino = (fuse_ino_t) inode;
	}

	inode->attr.st_uid = fs.uid;
	inode->attr.st_gid = fs.gid;
	inode->attr.st_nlink = 1;
}

static struct inode *inode_new(struct inode *parent)
{
	struct inode *inode = new struct inode();

	inode_init(inode, parent);

	return inode;
}

static void file_inode_from_digest(struct inode *inode, const Digest& digest, bool is_executable)
{
	inode->attr.st_mode = S_IFREG | 0644;
	inode->attr.st_size = digest.size_bytes();

	if (is_executable) {
		inode->attr.st_mode |= 0111;
	}

	inode->digest = digest;
}

static void dir_inode_from_digest(struct inode *inode, const Digest& digest)
{
	inode->attr.st_mode = S_IFDIR | 0755;

	inode->digest = digest;
}

static struct inode *get_open_inode(fuse_ino_t inode_id)
{
	struct inode *inode = get_inode(inode_id);

	if (S_ISDIR(inode->attr.st_mode) && !inode->dentries_populated) {
		int fd = fd_from_digest(inode->digest);
		if (fd < 0) {
			return NULL;
		}

		std::string fdpath("/dev/fd/");
		fdpath.append(std::to_string(fd));
		std::fstream dirstream(fdpath, std::ios::in | std::ios::binary);
		close(fd);
		if (!dirstream) {
			return NULL;
		}

		Directory directory;
		if (!directory.ParseFromIstream(&dirstream)) {
			return NULL;
		}

		for (int i = 0; i < directory.files_size(); i++) {
			const FileNode& file_node = directory.files(i);
			struct inode *file_inode = inode_new(inode);
			file_inode_from_digest(file_inode, file_node.digest(), file_node.is_executable());
			inode->dentries[file_node.name()] = file_inode;
		}

		for (int i = 0; i < directory.directories_size(); i++) {
			const DirectoryNode& directory_node = directory.directories(i);
			struct inode *subdir_inode = inode_new(inode);
			dir_inode_from_digest(subdir_inode, directory_node.digest());
			inode->dentries[directory_node.name()] = subdir_inode;
		}

		for (int i = 0; i < directory.symlinks_size(); i++) {
			const SymlinkNode& symlink_node = directory.symlinks(i);
			struct inode *symlink_inode = inode_new(inode);
			symlink_inode->attr.st_mode = S_IFLNK | 0644;
			symlink_inode->symlink_target = symlink_node.target();
			inode->dentries[symlink_node.name()] = symlink_inode;
		}

		inode->dentries_populated = true;
	} else if (S_ISREG(inode->attr.st_mode) && inode->fd < 0) {
		/* regular file */

		if (fs.n_open_inodes >= MAX_OPEN_INODES) {
			struct inode *open_inode = fs.open_inodes->prev;
			close_inode(open_inode);
		}

		if (inode->modified) {
			inode->fd = open(inode->temppath.c_str(), O_RDWR);
		} else {
			inode->fd = fd_from_digest(inode->digest);
		}
		if (inode->fd < 0) {
			//fprintf(stderr, "unable to open `%s`: %m\n", inode->path);
			return NULL;
		}

		if (fs.n_open_inodes == 0) {
			inode->next = inode;
			inode->prev = inode;
		} else {
			inode->next = fs.open_inodes;
			inode->prev = fs.open_inodes->prev;
			inode->prev->next = inode;
			inode->next->prev = inode;
		}
		fs.open_inodes = inode;
		fs.n_open_inodes++;
	}

	return inode;
}

static void fs_init(void *userdata, struct fuse_conn_info *conn)
{
	if (conn->capable & FUSE_CAP_SPLICE_MOVE) {
		conn->want |= FUSE_CAP_SPLICE_MOVE;
	}
	if (conn->capable & FUSE_CAP_SPLICE_WRITE) {
		conn->want |= FUSE_CAP_SPLICE_WRITE;
	}
	if (conn->capable & FUSE_CAP_NO_OPEN_SUPPORT) {
		conn->want |= FUSE_CAP_NO_OPEN_SUPPORT;
	}

	/* incompatible with FUSE_CAP_NO_OPEN_SUPPORT */
	conn->want &= ~FUSE_CAP_ATOMIC_O_TRUNC;
}

static int fs_do_lookup(fuse_req_t req, fuse_ino_t parent_id, const char *name, struct fuse_entry_param *e)
{
	struct inode *parent = get_open_inode(parent_id);

	if (!parent) {
		return -errno;
	}

#ifdef DEBUG
	fprintf(stderr, "lookup %s\n", name);
#endif

	/* no external changes happen to underlying filesystem */
	e->attr_timeout = 24 * 3600;
	e->entry_timeout = 24 * 3600;

	auto it = parent->dentries.find(name);
	if (it == parent->dentries.end() || !it->second) {
		/* No matching directory entry
		 * Returning entry with inode 0 instead of ENOENT
		 * enables negative dentry caching.
		 */
#ifdef DEBUG
		std::cerr << "lookup failed for " << name << "\n";
#endif
		return 0;
	}

	struct inode *inode = it->second;

	memcpy(&e->attr, &inode->attr, sizeof(e->attr));
	e->ino = (fuse_ino_t) inode;

	return 0;
}

static void fs_lookup(fuse_req_t req, fuse_ino_t parent_id, const char *name)
{
	struct fuse_entry_param e = {0};

	int ret = fs_do_lookup(req, parent_id, name, &e);

	if (ret == 0) {
		fuse_reply_entry(req, &e);
	} else {
		fuse_reply_err(req, -ret);
	}
}

static void fs_getattr(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	struct inode *inode = get_inode(ino);

	fuse_reply_attr(req, &inode->attr, 24 * 3600);
}

static void inode_set_modified(struct inode *inode)
{
	if (!inode->modified) {
		if (inode->parent) {
			inode_set_modified(inode->parent);
		}
		inode->modified = true;
	}
}

static int inode_copy_on_write(struct inode *inode)
{
	assert(S_ISREG(inode->attr.st_mode));

	if (inode->modified) {
		/* new file or already copied */
		return 0;
	}

	std::string temppath("/dev/fd/");
	temppath.append(std::to_string(fs.local_dfd));
	temppath.append("/tmp/XXXXXX");
	int fd = mkstemp(&temppath[0]);
	if (fd < 0) {
		return -1;
	}

	char *buffer = new char[65536];
	ssize_t count = 0;
	for (off_t offset = 0; offset < inode->attr.st_size; offset += count) {
		count = pread(inode->fd, buffer, sizeof(*buffer), offset);
		if (count < 0 || write(fd, buffer, count) < 0) {
			close(fd);
			return -1;
		}
	}

	close(inode->fd);
	inode->fd = fd;

	inode->temppath = temppath;

	inode_set_modified(inode);

	return 0;
}

static void inode_update_mtime(struct inode *inode)
{
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);

	inode->attr.st_mtime = ts.tv_sec;
	inode->attr.st_atime = ts.tv_sec;
	inode->attr.st_ctime = ts.tv_sec;
}

static void fs_setattr(fuse_req_t req, fuse_ino_t ino, struct stat *attr, int valid, struct fuse_file_info *fi)
{
	struct inode *inode = get_open_inode(ino);

	if (valid & FUSE_SET_ATTR_MODE) {
		if (S_ISREG(inode->attr.st_mode)) {
			/* the only supported mode change is the executable bit for regular files */
			/* the executable bit is stored in the parent directory */
			inode_set_modified(inode->parent);

			inode->attr.st_mode &= ~0111;

			if (attr->st_mode & 0100) {
				/* executable */
				inode->attr.st_mode |= 0111;
			}
		}
	}

	if (valid & FUSE_SET_ATTR_SIZE) {
		if (inode_copy_on_write(inode) < 0) {
			fuse_reply_err(req, errno);
			return;
		}

		if (ftruncate(inode->fd, attr->st_size) < 0) {
			fuse_reply_err(req, errno);
			return;
		}
		inode->attr.st_size = attr->st_size;
	}

	if (valid & FUSE_SET_ATTR_MTIME_NOW) {
		inode_update_mtime(inode);
	} else if (valid & FUSE_SET_ATTR_MTIME) {
		inode->attr.st_mtime = attr->st_mtime;
	}

	fs_getattr(req, ino, fi);
}

static void fs_readlink(fuse_req_t req, fuse_ino_t ino)
{
	struct inode *inode = get_inode(ino);

	if (!S_ISLNK(inode->attr.st_mode)) {
		/* not a symlink */
		fuse_reply_err(req, EINVAL);
		return;
	}

	fuse_reply_readlink(req, inode->symlink_target.c_str());
}

static void fs_opendir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	struct inode *inode = get_open_inode(ino);

	struct opendir *d = new struct opendir();
	if (d == NULL) {
		fuse_reply_err(req, ENOMEM);
		return;
	}

	d->iterator = inode->dentries.begin();

	fi->fh = (uintptr_t) d;
	fuse_reply_open(req, fi);
}

static void fs_do_readdir(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi, int plus)
{
	struct opendir *d = (struct opendir *) fi->fh;
	char *buf;
	char *p;
	size_t rem;
	int err;

	struct inode *inode = get_open_inode(ino);
	if (!inode) {
		fuse_reply_err(req, errno);
		return;
	}

	buf = (char *) calloc(size, 1);
	if (!buf) {
		fuse_reply_err(req, ENOMEM);
		return;
	}

	if (offset != d->offset) {
		/* Offset changed since last readdir */
		if (offset != 0) {
			/* Rewind is the only supported seek operation */
			fuse_reply_err(req, EINVAL);
			return;
		}

		d->offset = 0;
	}

	if (d->offset == 0) {
		d->iterator = inode->dentries.begin();
	}

	p = buf;
	rem = size;
	while (1) {
		size_t entsize;
		off_t nextoff;

		if (d->iterator == inode->dentries.end()) {
			break;
		}

		if (!d->iterator->second) {
			/* deleted entry, continue */
			d->iterator++;
			d->offset++;
			continue;
		}

		const char *name = d->iterator->first.c_str();
		struct inode *child_inode = d->iterator->second;
		nextoff = d->offset + 1;

		if (plus) {
			struct fuse_entry_param e;

			int ret = fs_do_lookup(req, ino, name, &e);
			if (ret) {
				err = -ret;
				goto error;
			}

			entsize = fuse_add_direntry_plus(req, p, rem,
							 name,
							 &e, nextoff);
		} else {
			entsize = fuse_add_direntry(req, p, rem,
						    name,
						    &child_inode->attr, nextoff);
		}
		if (entsize > rem) {
			/* Insufficient space in buffer */
			break;
		}

		p += entsize;
		rem -= entsize;

		d->iterator++;
		d->offset++;
	}

	fuse_reply_buf(req, buf, size - rem);
	free(buf);
	return;

error:
	free(buf);
	fuse_reply_err(req, err);
}

static void fs_readdir(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi)
{
	fs_do_readdir(req, ino, size, offset, fi, 0);
}

static void fs_readdirplus(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi)
{
	fs_do_readdir(req, ino, size, offset, fi, 1);
}

static void fs_releasedir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	struct opendir *d = (struct opendir *) fi->fh;
	delete d;
	fuse_reply_err(req, 0);
}

static void fs_read(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi)
{
	struct fuse_bufvec buf = FUSE_BUFVEC_INIT(size);

	struct inode *inode = get_open_inode(ino);

	buf.buf[0].flags = fuse_buf_flags(FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK);
	buf.buf[0].fd = inode->fd;
	buf.buf[0].pos = offset;

	fuse_reply_data(req, &buf, FUSE_BUF_SPLICE_MOVE);
}

static void fs_write_buf(fuse_req_t req, fuse_ino_t ino, struct fuse_bufvec *in_buf, off_t offset, struct fuse_file_info *fi)
{
	struct fuse_bufvec out_buf = FUSE_BUFVEC_INIT(fuse_buf_size(in_buf));

	struct inode *inode = get_open_inode(ino);

	if (inode_copy_on_write(inode) < 0) {
		fuse_reply_err(req, errno);
		return;
	}

	out_buf.buf[0].flags = fuse_buf_flags(FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK);
	out_buf.buf[0].fd = inode->fd;
	out_buf.buf[0].pos = offset;

	ssize_t res = fuse_buf_copy(&out_buf, in_buf, fuse_buf_copy_flags(0));
	if (res < 0) {
		fuse_reply_err(req, -res);
	} else {
		if (offset + res > inode->attr.st_size) {
			inode->attr.st_size = offset + res;
		}
		inode_update_mtime(inode);
		fuse_reply_write(req, (size_t) res);
	}
}

static void fs_create(fuse_req_t req, fuse_ino_t parent_id, const char *name, mode_t mode, struct fuse_file_info *fi)
{
	struct inode *parent = get_open_inode(parent_id);

	auto it = parent->dentries.find(name);
	if (it != parent->dentries.end() && it->second) {
		fuse_reply_err(req, EEXIST);
		return;
	}

#ifdef DEBUG
	fprintf(stderr, "create %s\n", name);
#endif

	struct inode *inode = new struct inode();
	inode->parent = parent;
	inode->fd = -1;
	inode->attr.st_ino = (fuse_ino_t) inode;
	inode->attr.st_mode = S_IFREG | 0644;
	inode->attr.st_uid = fs.uid;
	inode->attr.st_gid = fs.gid;
	inode->attr.st_nlink = 1;

	if (mode & 0100) {
		/* executable */
		inode->attr.st_mode |= 0111;
	}

	std::string temppath("/dev/fd/");
	temppath.append(std::to_string(fs.local_dfd));
	temppath.append("/tmp/XXXXXX");
	int fd = mkstemp(&temppath[0]);
	if (fd < 0) {
		fuse_reply_err(req, errno);
		return;
	}
	close(fd);

	inode->temppath = temppath;

	inode_update_mtime(inode);
	inode_update_mtime(parent);
	inode_set_modified(inode);

	parent->dentries[name] = inode;

	struct fuse_entry_param e = {0};
	int ret = fs_do_lookup(req, parent_id, name, &e);

	if (ret == 0) {
		fuse_reply_create(req, &e, fi);
	} else {
		fuse_reply_err(req, -ret);
	}
}

static void fs_mkdir(fuse_req_t req, fuse_ino_t parent_id, const char *name, mode_t mode)
{
	struct inode *parent = get_open_inode(parent_id);

	auto it = parent->dentries.find(name);
	if (it != parent->dentries.end() && it->second) {
		fuse_reply_err(req, EEXIST);
		return;
	}

	struct inode *inode = inode_new(parent);
	inode->attr.st_mode = S_IFDIR | 0755;
	inode->dentries_populated = true;

	inode_update_mtime(inode);
	inode_update_mtime(parent);
	inode_set_modified(inode);

	parent->dentries[name] = inode;

	struct fuse_entry_param e = {0};
	int ret = fs_do_lookup(req, parent_id, name, &e);

	if (ret == 0) {
		fuse_reply_entry(req, &e);
	} else {
		fuse_reply_err(req, -ret);
	}
}

static void fs_symlink(fuse_req_t req, const char *link, fuse_ino_t parent_id, const char *name)
{
	struct inode *parent = get_open_inode(parent_id);

	auto it = parent->dentries.find(name);
	if (it != parent->dentries.end() && it->second) {
		fuse_reply_err(req, EEXIST);
		return;
	}

	struct inode *inode = inode_new(parent);
	inode->attr.st_mode = S_IFLNK | 0644;

	inode->symlink_target = link;

	inode_update_mtime(inode);
	inode_update_mtime(parent);
	inode_set_modified(inode);

	parent->dentries[name] = inode;

	struct fuse_entry_param e = {0};
	int ret = fs_do_lookup(req, parent_id, name, &e);

	if (ret == 0) {
		fuse_reply_entry(req, &e);
	} else {
		fuse_reply_err(req, -ret);
	}
}

static void fs_mknod(fuse_req_t req, fuse_ino_t parent_id, const char *name, mode_t mode, dev_t rdev)
{
	fuse_reply_err(req, EPERM);
}

static void fs_unlink(fuse_req_t req, fuse_ino_t parent_id, const char *name)
{
	struct inode *parent = get_open_inode(parent_id);

	auto it = parent->dentries.find(name);
	if (it == parent->dentries.end() || !it->second) {
		fuse_reply_err(req, ENOENT);
	} else {
		it->second = NULL;
		inode_update_mtime(parent);
		inode_set_modified(parent);
		fuse_reply_err(req, 0);
	}
}

static void fs_rmdir(fuse_req_t req, fuse_ino_t parent_id, const char *name)
{
	struct inode *parent = get_open_inode(parent_id);

	auto it = parent->dentries.find(name);
	if (it == parent->dentries.end() || !it->second) {
		fuse_reply_err(req, ENOENT);
	} else {
		it->second = NULL;
		inode_update_mtime(parent);
		inode_set_modified(parent);
		fuse_reply_err(req, 0);
	}
}

static void fs_rename(fuse_req_t req, fuse_ino_t parent_id, const char *name, fuse_ino_t newparent_id, const char *newname, unsigned int flags)
{
	struct inode *parent = get_open_inode(parent_id);
	struct inode *newparent = get_open_inode(newparent_id);

	auto it = parent->dentries.find(name);
	if (it == parent->dentries.end() || !it->second) {
		fuse_reply_err(req, ENOENT);
		return;
	}

	auto newit = newparent->dentries.find(newname);

#if 0
	bool newexists = newit != parent->dentries.end() && newit->second;

	if (flags & RENAME_EXCHANGE) {
		if (!newexists) {
			fuse_reply_err(req, ENOENT);
			return;
		}

		/* swap inodes */
		struct inode *inode1 = it->second;
		struct inode *inode2 = newit->second;
		it->second = inode2;
		newit->second = inode1;

		inode_set_modified(parent);
		fuse_reply_err(req, 0);
		return;
	}

	if ((flags & RENAME_NOREPLACE) && newexists) {
		fuse_reply_err(req, EEXIST);
		return;
	}
#else
	if (flags != 0) {
		fuse_reply_err(req, EINVAL);
		return;
	}
#endif

	if (newit == newparent->dentries.end()) {
		newparent->dentries[newname] = it->second;
	} else {
		newit->second = it->second;
	}
	it->second = NULL;
	inode_set_modified(parent);
	fuse_reply_err(req, 0);
}

static void fs_open(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	fuse_reply_err(req, ENOSYS);
}

static struct fuse_lowlevel_ops fs_oper = {
	.init		= fs_init,
	.destroy	= NULL,
	.lookup		= fs_lookup,
	.forget		= NULL,
	.getattr	= fs_getattr,
	.setattr	= fs_setattr,
	.readlink	= fs_readlink,
	.mknod		= fs_mknod,
	.mkdir		= fs_mkdir,
	.unlink		= fs_unlink,
	.rmdir		= fs_rmdir,
	.symlink	= fs_symlink,
	.rename		= fs_rename,
	.link		= NULL,
	.open		= fs_open,
	.read		= fs_read,
	.write		= NULL,
	.flush		= NULL,
	.release	= NULL,
	.fsync		= NULL,
	.opendir	= fs_opendir,
	.readdir	= fs_readdir,
	.releasedir	= fs_releasedir,
	.fsyncdir	= NULL,
	.statfs		= NULL,
	.setxattr	= NULL,
	.getxattr	= NULL,
	.listxattr	= NULL,
	.removexattr	= NULL,
	.access		= NULL,
	.create		= fs_create,
	.getlk		= NULL,
	.setlk		= NULL,
	.bmap		= NULL,
	.ioctl		= NULL,
	.poll		= NULL,
	.write_buf	= fs_write_buf,
	.retrieve_reply	= NULL,
	.forget_multi	= NULL,
	.flock		= NULL,
	.fallocate	= NULL,
	.readdirplus	= fs_readdirplus,
};

static int fs_opt_proc(void *data, const char *arg, int key, struct fuse_args *outargs)
{
	if (key == FUSE_OPT_KEY_NONOPT) {
		if (!fs.opts.mountpoint) {
			fs.opts.mountpoint = arg;
			return 0;
		} else {
			fprintf(stderr, "invalid argument `%s'\n", arg);
			return -1;
		}
	} else {
		/* pass through unknown options to fuse session */
		return 1;
	}
}

#define FS_OPT(t, p, v) { t, offsetof(struct fs_opts, p), v }

static struct fuse_opt fs_opts[] = {
	FS_OPT("--remote=%s", remote_url, 0),
	FS_OPT("--server-cert=%s", server_cert, 0),
	FS_OPT("--client-key=%s", client_key, 0),
	FS_OPT("--client-cert=%s", client_cert, 0),
	FS_OPT("--local=%s", local_path, 0),
	FS_OPT("--input-digest=%s", input_digest, 0),
	FS_OPT("--output-digest=%s", output_digest, 0),
	FS_OPT("--chdir=%s", chdir, 0),
	FUSE_OPT_END
};

static void usage(const char *name) {
	fprintf(stderr, "usage: %s [OPTIONS] MOUNTPOINT COMMAND [ARGS...]\n", name);
	fprintf(stderr, "    --remote=URL           URL for remote CAS server\n");
	fprintf(stderr, "    --server-cert=PATH     Public server certificate for TLS (PEM-encoded)\n");
	fprintf(stderr, "    --client-key=PATH      Private client key for TLS (PEM-encoded)\n");
	fprintf(stderr, "    --client-cert=PATH     Public client certificate for TLS (PEM-encoded)\n");
	fprintf(stderr, "    --local=PATH           Local CAS cache directory\n");
	fprintf(stderr, "    --input-digest=PATH    Path to input directory digest\n");
	fprintf(stderr, "    --output-digest=PATH   Path to output directory digest\n");
	fprintf(stderr, "    --chdir=PATH           Change directory to PATH\n");
	fuse_lowlevel_help();
}

static std::string get_file_contents(const char *filename)
{
	FILE *fp = fopen(filename, "rb");
	if (fp) {
		std::string contents;
		fseek(fp, 0, SEEK_END);
		contents.resize(ftell(fp));
		rewind(fp);
		fread(&contents[0], 1, contents.size(), fp);
		fclose(fp);
		return contents;
	}

	throw errno;
}

static void child_handler(int sig)
{
	siginfo_t si = {0};
	if (waitid(P_PID, fs.bwrap_pid, &si, WEXITED | WNOHANG) == 0) {
		if (si.si_pid == fs.bwrap_pid) {
			fs.bwrap_status = si.si_status;
			fuse_session_exit(fs.fuse);
		}
	}
}

static void ignore_signal(int sig)
{
	struct sigaction act = {0};
	act.sa_handler = SIG_IGN;
	sigemptyset(&act.sa_mask);
	sigaction(sig, &act, NULL);
}

static void install_signal_handlers(void)
{
	/* do not terminate before command child process terminates */
	ignore_signal(SIGHUP);
	ignore_signal(SIGINT);
	ignore_signal(SIGTERM);
	ignore_signal(SIGPIPE);

	struct sigaction act = {0};
	act.sa_handler = child_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_NOCLDSTOP;
	sigaction(SIGCHLD, &act, NULL);
}

static void flush_to_cas(struct inode *inode)
{
	if (!inode->modified) {
		return;
	}

	if (S_ISDIR(inode->attr.st_mode)) {
		/* directory */

		Directory directory;

		for (auto it = inode->dentries.begin(); it != inode->dentries.end(); it++) {
			struct inode *child_inode = it->second;
			if (child_inode) {
				flush_to_cas(child_inode);

				if (S_ISDIR(child_inode->attr.st_mode)) {
					auto subdir_node = directory.add_directories();
					subdir_node->set_name(it->first);
					subdir_node->mutable_digest()->CopyFrom(child_inode->digest);
				} else if (S_ISREG(child_inode->attr.st_mode)) {
					auto file_node = directory.add_files();
					file_node->set_name(it->first);
					file_node->mutable_digest()->CopyFrom(child_inode->digest);
					file_node->set_is_executable((child_inode->attr.st_mode & 0100) != 0);
				} else if (S_ISLNK(child_inode->attr.st_mode)) {
					auto symlink_node = directory.add_symlinks();
					symlink_node->set_name(it->first);
					symlink_node->set_target(child_inode->symlink_target);
				} else {
					assert(0);
				}
			}
		}

		std::string temppath("/dev/fd/");
		temppath.append(std::to_string(fs.local_dfd));
		temppath.append("/tmp/XXXXXX");
		int fd = mkstemp(&temppath[0]);
		if (fd < 0) {
			return;
		}
		close(fd);
		std::fstream out(temppath.c_str(), std::ios::out | std::ios::binary);
		directory.SerializeToOstream(&out);
		out.close();

		inode->temppath = temppath;
	}

	if (S_ISREG(inode->attr.st_mode) || S_ISDIR(inode->attr.st_mode)) {
		/* directory or regular file */

		/* Checksum file */

		int fd = open(inode->temppath.c_str(), O_RDONLY);
		assert(fd >= 0);

		char *buffer = new char[65536];

		unsigned char hash[SHA256_DIGEST_LENGTH];
		SHA256_CTX sha256;
		SHA256_Init(&sha256);
		ssize_t bytes_read;
		ssize_t total_bytes_read = 0;
		while ((bytes_read = read(fd, buffer, 65536)) > 0) {
			SHA256_Update(&sha256, buffer, bytes_read);
			total_bytes_read += bytes_read;
		}
		SHA256_Final(hash, &sha256);

		std::stringstream ss;
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
			ss << std::hex << std::setw(2) << std::setfill('0') << (int) hash[i];
		}

		inode->digest.set_hash(ss.str());
		inode->digest.set_size_bytes(total_bytes_read);

		/* Move it to the right place */

		mkdirat(fs.local_dfd, objdir(inode->digest).c_str(), 0755);
		if (renameat(fs.local_dfd, inode->temppath.c_str(), fs.local_dfd, objpath(inode->digest).c_str()) < 0) {
			return;
		}

		inode->modified = false;

		/* Upload to CAS server */

		if (fs.opts.remote_url) {
			std::string resource_name;
			resource_name.append(inode->digest.hash());
			resource_name.append("/");
			resource_name.append(std::to_string(inode->digest.size_bytes()));

			lseek(fd, 0, SEEK_SET);

			grpc::ClientContext context;
			WriteResponse response;
			auto writer = fs.bytestream_client->Write(&context, &response);
			ssize_t offset = 0;
			while ((bytes_read = read(fd, buffer, 65536)) > 0) {
				WriteRequest request;
				request.set_resource_name(resource_name);
				request.set_write_offset(offset);
				request.set_data(buffer, bytes_read);
				offset += bytes_read;
				if (offset < inode->digest.size_bytes()) {
					writer->Write(request);
				} else {
					request.set_finish_write(true);
					writer->WriteLast(request, grpc::WriteOptions());
				}
			}
		}

		delete[] buffer;
		close(fd);
	}
}

int main(int argc, char *argv[])
{
	/* scan argv to determine number of arguments before command
	 * to limit fuse_opt_parse() */
	int fuse_argc;
	for (fuse_argc = 1; fuse_argc < argc; fuse_argc++) {
		if (argv[fuse_argc][0] == '-') {
			if (argv[fuse_argc][1] == 'o') {
				/* FUSE option -o */
				/* include option value */
				fuse_argc++;
				if (fuse_argc >= argc) {
					/* missing option value */
					break;
				}
			}
		} else {
			/* mountpoint, last FUSE argument */
			fuse_argc++;
			break;
		}
	}

	fs.uid = getuid();
	fs.gid = getgid();

	struct fuse_args args = FUSE_ARGS_INIT(fuse_argc, argv);

	if (fuse_opt_parse(&args, &fs.opts, fs_opts, fs_opt_proc) < 0) {
		usage(argv[0]);
		return 1;
	}

	if (!fs.opts.mountpoint) {
		fprintf(stderr, "Mountpoint is missing\n\n");
		usage(argv[0]);
		return 1;
	}

	if (!fs.opts.local_path) {
		fprintf(stderr, "Local CAS cache directory is missing\n\n");
		usage(argv[0]);
		return 1;
	}
	fs.local_dfd = open(fs.opts.local_path, O_RDONLY | O_DIRECTORY);
	if (fs.local_dfd < 0) {
		fprintf(stderr, "Failed to open %s: %m\n", fs.opts.local_path);
		return 1;
	}
	mkdirat(fs.local_dfd, "objects", 0755);
	mkdirat(fs.local_dfd, "tmp", 0755);

	if (fs.opts.remote_url) {
		std::string target;
		std::shared_ptr<grpc::ChannelCredentials> creds;
		if (strncmp(fs.opts.remote_url, "http://", strlen("http://")) == 0) {
			target = fs.opts.remote_url + strlen("http://");
			creds = grpc::InsecureChannelCredentials();
		} else if (strncmp(fs.opts.remote_url, "https://", strlen("https://")) == 0) {
			auto options = grpc::SslCredentialsOptions();
			if (fs.opts.server_cert) {
				options.pem_root_certs = get_file_contents(fs.opts.server_cert);
			}
			if (fs.opts.client_key) {
				options.pem_private_key = get_file_contents(fs.opts.client_key);
			}
			if (fs.opts.client_cert) {
				options.pem_cert_chain = get_file_contents(fs.opts.client_cert);
			}
			target = fs.opts.remote_url + strlen("https://");
			creds = grpc::SslCredentials(options);
		} else {
			fprintf(stderr, "Unsupported URL scheme\n");
			return 1;
		}

		fs.channel = grpc::CreateChannel(target, creds);
		fs.bytestream_client = ByteStream::NewStub(fs.channel);
		fs.cas_client = ContentAddressableStorage::NewStub(fs.channel);
	}

	/* read digest of sandbox root directory (input tree) */
	Digest digest;
	if (!fs.opts.input_digest) {
		if (!digest.ParseFromIstream(&std::cin) || digest.hash().empty()) {
			std::cerr << "Failed to parse digest\n";
			return 1;
		}
	} else {
		std::fstream fstream(fs.opts.input_digest, std::ios::in | std::ios::binary);
		if (!digest.ParseFromIstream(&fstream) || digest.hash().empty()) {
			std::cerr << "Failed to parse digest\n";
			return 1;
		}
		fstream.close();
	}
	inode_init(&fs.root_inode, NULL);
	dir_inode_from_digest(&fs.root_inode, digest);

	fs.fuse = fuse_session_new(&args, &fs_oper, sizeof(fs_oper), &fs);
	if (fs.fuse == NULL) {
		return 1;
	}

	if (fuse_argc == argc) {
		/* No command specified, mount but don't execute bubblewrap */

		if (fuse_set_signal_handlers(fs.fuse) != 0) {
			return 1;
		}

		if (fuse_session_mount(fs.fuse, fs.opts.mountpoint) != 0) {
			return 1;
		}
	} else {
		/* Command specified */

		install_signal_handlers();

		if (fuse_session_mount(fs.fuse, fs.opts.mountpoint) != 0) {
			return 1;
		}

		fs.bwrap_pid = fork();
		if (fs.bwrap_pid < 0) {
			return 1;
		}

		if (fs.bwrap_pid == 0) {
			/* bwrap child */

			if (!fs.opts.input_digest) {
				/* stdin is used for input directory digest
				 * open /dev/null for stdin
				 */
				int null_fd = open("/dev/null", O_RDONLY);
				dup2(null_fd, 0);
				close(null_fd);
			}

			if (!fs.opts.output_digest) {
				/* stdout is used for output directory digest
				 * redirect stdout to stderr for now
				 */
				dup2(2, 1);
			}

			std::vector<const char *> bwrap_argv;
			bwrap_argv.push_back("bwrap");

			/* Create a new pid namespace, this also ensures that any subprocesses
			 * are cleaned up when the bwrap process exits.
			 */
			bwrap_argv.push_back("--unshare-pid");

			/* Ensure subprocesses are cleaned up when the bwrap parent dies. */
			bwrap_argv.push_back("--die-with-parent");

			/* Mount FUSE as sandbox rootfs */
			bwrap_argv.push_back("--bind");
			bwrap_argv.push_back(fs.opts.mountpoint);
			bwrap_argv.push_back("/");

			/* Disable network access */
			bwrap_argv.push_back("--unshare-net");
			bwrap_argv.push_back("--unshare-uts");
			bwrap_argv.push_back("--hostname");
			bwrap_argv.push_back("buildbox");
			bwrap_argv.push_back("--unshare-ipc");

			if (fs.opts.chdir) {
				bwrap_argv.push_back("--chdir");
				bwrap_argv.push_back(fs.opts.chdir);
			}

			/* Give it a proc and tmpfs */
			bwrap_argv.push_back("--proc");
			bwrap_argv.push_back("/proc");
			bwrap_argv.push_back("--tmpfs");
			bwrap_argv.push_back("/tmp");

			static const char *devices[] = {
				"/dev/full",
				"/dev/null",
				"/dev/urandom",
				"/dev/random",
				"/dev/zero",
				NULL
			};
			for (const char **device = devices; *device; device++) {
				bwrap_argv.push_back("--dev-bind");
				bwrap_argv.push_back(*device);
				bwrap_argv.push_back(*device);
			}

			bwrap_argv.push_back("--unshare-user");

			/* append command */
			for (int i = fuse_argc; i < argc; i++) {
				bwrap_argv.push_back(argv[i]);
			}

			bwrap_argv.push_back(NULL);

			execvp("bwrap", const_cast<char **>(&bwrap_argv[0]));
			_exit(1);
		}
	}

	std::fstream output_digest_stream;
	if (fs.opts.output_digest) {
		output_digest_stream.open(fs.opts.output_digest, std::ios::out | std::ios::trunc | std::ios::binary);
	}

	fuse_daemonize(1 /* foreground */);

	fuse_session_loop(fs.fuse);

	/* Recalculate digests and push changes to CAS */
	flush_to_cas(&fs.root_inode);

	/* Output new digest of root directory */
	if (!fs.opts.output_digest) {
		if (!fs.root_inode.digest.SerializeToOstream(&std::cout)) {
			std::cerr << "Failed to serialize digest\n";
			return 1;
		}
	} else {
		if (!fs.root_inode.digest.SerializeToOstream(&output_digest_stream)) {
			std::cerr << "Failed to serialize digest\n";
			return 1;
		}
		output_digest_stream.close();
	}

	fuse_session_unmount(fs.fuse);
	fuse_session_destroy(fs.fuse);
	fuse_opt_free_args(&args);

	return fs.bwrap_status;
}
